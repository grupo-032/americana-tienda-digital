package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AmericanaTiendaDigitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(AmericanaTiendaDigitalApplication.class, args);
	}

}
