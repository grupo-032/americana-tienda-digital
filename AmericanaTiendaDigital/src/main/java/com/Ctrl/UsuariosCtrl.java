package com.Ctrl;

//import com.Services.PersonasService;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.Services.UsuariosService;
import com.TO.Usuarios;

@Controller
public class UsuariosCtrl {

    @Autowired
    private UsuariosService usuariosService;

    //@Autowired
   // private PersonasService personasService;

    /*METODOS PARA POSTMAN*/
    @PostMapping("/usuarios/registrar")
    public ResponseEntity<Usuarios> registrar(@RequestBody Usuarios usuario) {
        return new ResponseEntity<>(usuariosService.guardar(usuario), HttpStatus.OK);
    }
    
    @CrossOrigin()
    @RequestMapping(value="/usuarios/{id}", method=RequestMethod.DELETE)
    public ResponseEntity<String> eliminar(@PathVariable int id) {
        Usuarios usr = usuariosService.buscarUsuario(id);
        if (usr != null) {
            usuariosService.eliminar(id);
            return new ResponseEntity<>("Registro eliminado", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Registro no encontrado", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /*METODOS PARA LA PAGINA HTML*/
    @CrossOrigin()
    @GetMapping("/usuarios")
    public ResponseEntity<List<Usuarios>> listarUsuarios() {
        List<Usuarios> usuarios = usuariosService.listarusuarios();
        return new ResponseEntity<>(usuarios, HttpStatus.OK) ;
    }
    
    @CrossOrigin()
    @PostMapping("/usuarios")
    public ResponseEntity<Usuarios> crear(@RequestBody Usuarios usuario) {
        return new ResponseEntity<>(usuariosService.guardar(usuario), HttpStatus.CREATED) ;
    }
    
    @CrossOrigin()
    @PutMapping("/usuarios")
    public ResponseEntity<Usuarios> actualizar(@RequestBody Usuarios usuario) {
        return new ResponseEntity<>(usuariosService.guardar(usuario), HttpStatus.CREATED) ;
    }


}
