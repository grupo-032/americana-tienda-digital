const url = "http://localhost:8080/personas/"
const contenedor = document.querySelector('tbody')
let resultados = ''

const modalpersonas = new bootstrap.Modal(document.getElementById('modalPersona'))
const formPersonas = document.querySelector('form')
const tipoCliente = document.getElementById('tipdoc')
const idPersona = document.getElementById('id')
const nombresPersona = document.getElementById('nombre')
const apellidosPersona = document.getElementById('email')
let opcion = ''

btnCrear.addEventListener('click', () => {
    idPersona.value = ''
    nombresPersona.value = ''
    apellidosPersona.value = ''
    idPersona.disabled = false
    modalpersonas.show()
    opcion = 'crear'
})

//funcion para mostrar resultados

const mostrar = (personas) => {
    personas.forEach(persona => {
        resultados += `<tr>
        <td >${persona.idPersonas}</td>
                        <td >${persona.nombres}</td>
                        <td >${persona.apellidos}</td>
                        <td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td>
                    </tr>`
    })

    contenedor.innerHTML = resultados
}

//procedimiento mostrar registros
fetch(url)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error))

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode
    const id = fila.firstElementChild.innerHTML
    console.log(id)

    alertify.confirm("Desea eliminar la Persona " + id,
        function () {
            fetch(url + id, {
                method: 'DELETE'
            })
                .then(() => location.reload())
        },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0
on(document, 'click', '.btnEditar', e => {

    const fila = e.target.parentNode.parentNode

    idForm = fila.children[0].innerHTML
    const nombre = fila.children[1].innerHTML
    const email = fila.children[2].innerHTML
    idPersona.value = idForm
    idPersona.disabled = true
    nombresPersona.value = nombre
    apellidosPersona.value = email

    opcion = 'editar'
    modalpersonas.show()
})

formPersonas.addEventListener('submit', (e) => {
    e.preventDefault()

    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idPersonas: idPersona.value,
                nombres: nombresPersona.value,
                apellidos: apellidosPersona.value,
                tipoDoc: '',
                documento: '',
                telefono: '',
                direccion: '',
                email: ''
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevoPersona = []
                nuevoPersona.push(data)
                mostrar(nuevoPersona)
            })
    }
    if (opcion == 'editar') {

        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idPersonas: idForm,
                nombres: nombresPersona.value,
                apellidos: apellidosPersona.value,
                tipoDoc: '',
                documento: '',
                telefono: '',
                direccion: '',
                email: ''
            })
        })
            .then(response => location.reload())

    }
    modalpersonas.hide()

})
