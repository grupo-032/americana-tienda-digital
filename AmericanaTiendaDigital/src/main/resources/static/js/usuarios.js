const url = "http://localhost:8080/usuarios/"
const contenedor = document.querySelector('tbody')
let resultados = ''

const modalusuarios = new bootstrap.Modal(document.getElementById('modalusuario'))
const formusuarios = document.querySelector('form')
const tipoCliente = document.getElementById('tipdoc')
const idusuario = document.getElementById('id')
const nombresusuario = document.getElementById('nombre')
const apellidosusuario = document.getElementById('email')
let opcion = ''

btnCrear.addEventListener('click', () => {
    idusuario.value = ''
    nombresusuario.value = ''
    apellidosusuario.value = ''
    idusuario.disabled = false
    modalusuarios.show()
    opcion = 'crear'
})

//funcion para mostrar resultados

const mostrar = (usuarios) => {
    usuarios.forEach(usuario => {
        resultados += `<tr>
        <td >${usuario.idUsuarios}</td>
                        <td >${usuario.usuario}</td>
                        <td >${usuario.clave}</td>
                        <td class="text-center" width="20%"><a class="btnEditar btn btn-primary">Editar</a><a class="btnBorrar btn btn-danger">Borrar</a></td>
                    </tr>`
    })

    contenedor.innerHTML = resultados
}

//procedimiento mostrar registros
fetch(url)
    .then(response => response.json())
    .then(data => mostrar(data))
    .catch(error => console.log(error))

const on = (element, event, selector, handler) => {
    element.addEventListener(event, e => {
        if (e.target.closest(selector))
            handler(e)
    })
}

on(document, 'click', '.btnBorrar', e => {
    const fila = e.target.parentNode.parentNode
    const id = fila.firstElementChild.innerHTML
    console.log(id)

    alertify.confirm("Desea eliminar la usuario " + id,
        function () {
            fetch(url + id, {
                method: 'DELETE'
            })
                .then(() => location.reload())
        },
        function () {
            alertify.error('Cancel')
        });
})

let idForm = 0
on(document, 'click', '.btnEditar', e => {

    const fila = e.target.parentNode.parentNode

    idForm = fila.children[0].innerHTML
    const nombre = fila.children[1].innerHTML
    const email = fila.children[2].innerHTML
    idusuario.value = idForm
    idusuario.disabled = true
    nombresusuario.value = nombre
    apellidosusuario.value = email

    opcion = 'editar'
    modalusuarios.show()
})

formusuarios.addEventListener('submit', (e) => {
    e.preventDefault()

    if (opcion == 'crear') {
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idUsuarios: idusuario.value,
                usuario: nombresusuario.value,
                clave: apellidosusuario.value,
                rol: '',
                idPersonas: 1
            })
        })
            .then(response => response.json())
            .then(data => {
                const nuevousuario = []
                nuevousuario.push(data)
                mostrar(nuevousuario)
            })
    }
    if (opcion == 'editar') {

        fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idUsuarios: idForm,
                usuario: nombresusuario.value,
                clave: apellidosusuario.value,
                rol: '',
                idPersonas: 1
            })
        })
            .then(response => location.reload())

    }
    modalusuarios.hide()

})
